<?php

namespace Zalmoksis\DataStructures\Tests;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;

class CollectionTest extends TestCase {

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, new class extends Collection {});
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, new class extends Collection {});
    }

    function testIfDefaultCollectionIsEmpty(): void {
        $this->assertCount(0, new class extends Collection {});
    }

    function testCountingEmptyCollection(): void {
        $this->assertCount(0, new class extends Collection {
            protected array $elements = [];
        });
    }

    function testCountingCollectionWithElements(): void {
        $this->assertCount(3, new class extends Collection {
            protected array $elements = [1, 'two', []];
        });
    }

    function testIteratingWithForeach(): void {
        $collection = new class extends Collection {
            protected array $elements = [true, 2, 'three'];
        };

        foreach ($collection as $index => $element) {
            $this->assertSame([true, 2, 'three'][$index], $element);
        }

        // iterating twice to make sure rewind works
        foreach ($collection as $index => $element) {
            $this->assertSame([true, 2, 'three'][$index], $element);
        }
    }
}
