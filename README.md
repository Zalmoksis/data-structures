# Data structures

[![pipeline status](https://gitlab.com/Zalmoksis/data-structures/badges/master/pipeline.svg)](https://gitlab.com/Zalmoksis/data-structures/-/commits/master)
[![coverage report](https://gitlab.com/Zalmoksis/data-structures/badges/master/coverage.svg)](https://gitlab.com/Zalmoksis/data-structures/-/commits/master)

Basic common data structures.

Currently inludes:
 - collection
