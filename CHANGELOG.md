# Change log

All notable changes to this project will be documented in this file
in a format based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

The project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0).

## [0.4.6] — 2022-06-11
### Added
- Testing PHP 8.1 in CI
- Labels in benchmark
### Changed
- Looser versioning of Psalm

## [0.4.5] — 2020-12-14
### Added
- Static analysis with Psalm + coding standard fixes
### Changed
- Improved CI
### Fixed
- Bumped licence year

## [0.4.4] — 2020-12-12
### Changed
- Optimized CI

## [0.4.3] — 2020-12-12
### Changed
- Improved configuration of Composer and PHP Unit

## [0.4.2] — 2020-12-11
### Changed
- Improved Composer configuration

## [0.4.1] — 2020-12-09
### Changed
- Improved configuration of PHP Unit and Gitlab CI
- Improved unit tests

## [0.4.0] — 2020-12-08
### Changed
- Collection implements `IteratorAggregate` instead of `Iterator` now

## [0.3.1] — 2020-12-07
### Added
- Allowing PHP 8.0
- More testing in Gitlab CI

## [0.3.0] — 2019-11-30
### Changed
- PHP version update to 7.4
- PHP Unit update to 8.*
- Collection now explicitly defined as abstract

## [0.2.0] — 2019-11-29
### Added
- This changelog
### Changed
- Internal implementation of Collection
- Slightly improved unit tests
- Slightly improved configuration of PHP Unit
- Slightly improved configuration of Composer
### Fixed
- Namespace in the unit test

## [0.1.4] — 2018-10-13
### Added
- MIT licence

## [0.1.3] — 2018-10-13
### Added
- Gitlab CI integration
- PHP Unit integration
- Unit tests

## [0.1.2] — 2018-07-17
### Added
- Collection implements Countable interface

## [0.1.1] — 2018-01-26
### Added
- README.md

## [0.1.0] — 2018-01-26
### Added
- Initial implementation
