<?php

const ITERATIONS = 100000;

class CollectionIterator implements Iterator {
    protected array $elements = [];

    function __construct(array $elements)
    {
        $this->elements = $elements;
    }

    // Iterator interface

    function current() {
        return current($this->elements);
    }

    function key() {
        return key($this->elements);
    }

    function next() {
        next($this->elements);
    }

    function rewind() {
        reset($this->elements);
    }

    function valid() {
        return key($this->elements) !== null;
    }
}

class CollectionIteratorAggregate implements IteratorAggregate {
    protected array $elements = [];

    function __construct(array $elements)
    {
        $this->elements = $elements;
    }

    // IteratorAggregate interface

    function getIterator()
    {
        return new ArrayIterator($this->elements);
    }
}

$elements = [
    new DateTime(),
    new DateTime(),
    new DateTime(),
    new DateTime(),
    new DateTime(),
    new DateTime(),
    new DateTime(),
    new DateTime(),
    new DateTime(),
    new DateTime(),
];

$microtime = microtime(true);

for ($i = 0; $i < ITERATIONS; $i++) {
    foreach($elements as $key => $element) {
        // nothing
    }
}

echo str_pad('array: ', 29, ' ') . (microtime(true) - $microtime) . "\n";

$microtime = microtime(true);

for ($i = 0; $i < ITERATIONS; $i++) {
    foreach(new CollectionIterator($elements) as $key => $element) {
        // nothing
    }
}

echo str_pad('CollectionIterator: ', 29, ' ') . (microtime(true) - $microtime) . "\n";

$microtime = microtime(true);

for ($i = 0; $i < ITERATIONS; $i++) {
    foreach(new CollectionIteratorAggregate($elements) as $key => $element) {
        // nothing
    }
}

echo str_pad('CollectionIteratorAggregate: ', 29, ' ') . (microtime(true) - $microtime) . "\n";
