<?php

namespace Zalmoksis\DataStructures;

use ArrayIterator;
use Countable;
use Iterator;
use IteratorAggregate;

abstract class Collection implements IteratorAggregate, Countable {
    protected array $elements = [];

    // IteratorAggregate interface

    function getIterator(): Iterator {
        return new ArrayIterator($this->elements);
    }

    // Countable interface

    function count() {
        return count($this->elements);
    }
}
